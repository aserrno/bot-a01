const binance_api = {
    API: '<< APIKEY >>',
    SECRET: '<< SECRET KEY >>'
}

const params = {
    interval: '1d'
}

module.exports = {
    binance_api: binance_api,
    params: params
}