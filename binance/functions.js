
const myConfig = require('../config_binance_api/config');
const Binance = require('node-binance-api');
const binance = new Binance().options({
  APIKEY: myConfig.binance_api.API,
  APISECRET: myConfig.binance_api.SECRET
});
const RSI = require('technicalindicators').RSI;
var allSymbols = [];

async function getAllSymbols() {
  let ticker = await binance.prices();
  for( const prop in ticker) {
    var currency = prop.toString();
    if(currency.substring(currency.length - 4, currency.length) === 'USDT' &&
       currency.substring(currency.length - 6, currency.length) !== 'UPUSDT' &&
       currency.substring(currency.length - 8, currency.length) !== 'DOWNUSDT') {
      allSymbols.push(currency);
    }
  }
   allSymbols.forEach( item => {
    getDataCurrency(item);
  });
}

function getBalanceUSDT() {
  binance.balance((error, balance) => {
    if (error) return console.error(error);
    console.log('Balance USDT: ' + balance.USDT.available)
  });
}

async function getDataCurrency(symbol) {
  await binance.candlesticks(symbol, myConfig.params.interval, (error, ticks, symbol) => {
    var closes = [];
    for (let i = 0; i < ticks.length; i++) {
      closes.push(Number(ticks[i][1]));
    }
    calcRSI(symbol, closes);
  }, { limit: 100 });
}

function calcRSI(symbol, closes) {
  var result = RSI.calculate({ period: 6, values: closes });

  // ---- SHOW VALUES MINUS THAN 32
  if(result[result.length -1] < 32) {
    console.log('Last RSI of ' + symbol + ' is ' + result[result.length -1]);
  }

  // ---- SHOW VALUES MINUS THAT 32 AND UPPER TO 78
  // var res = result[result.length -1] < 32 ? '- Last RSI of ' + symbol + ' is ' + result[result.length -1]: result[result.length -1] > 78 ? '+ Last RSI of ' + symbol + ' is ' + result[result.length -1]: null;
  // if(res != null) {
  //   console.log(res)
  // }
}


module.exports = {
  getAllSymbols: getAllSymbols,
  getBalanceUSDT: getBalanceUSDT,
  getDataCurrency: getDataCurrency
}